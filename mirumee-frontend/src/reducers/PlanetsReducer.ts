import { PlanetAction } from "../actions/PlanetsActions";

export type Planet = {
    name: string,
	rotationPeriod: number | "unknown",
	orbitalPeriod: number | "unknown",
	diameter: number | "unknown",
	climate: string,
	gravity: string,
	terrain: string,
	surfaceWater: number | "unknown",
	population: number | "unknown",
}

export type Planets = {
    [key: string]: Planet;
}

export function planetsReducer(planets: Planets = {}, action: PlanetAction){
	if(action.type === "FETCH_PLANETS"){
		return {
			...action.payload,
			...planets
		}
	}
    return planets;
}