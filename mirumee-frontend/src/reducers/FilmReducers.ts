import { FilmActions } from '../actions/FilmsActions';

export type Film = {
    title: string,
    planets: Array<string>
}

export function fetchedFilmsReducer(fetchedFilms: Array<Film> = [], action: FilmActions) {
    if(action.type === "FETCH_FILMS"){
        return action.payload;
    }
    return fetchedFilms;
}

export function customFilmsReducer(customFilms: Array<Film> = [], action: FilmActions) {
    if(action.type === "LOAD_CUSTOM_FILMS"){
        return action.payload;
    }
    else if(action.type === "NEW_CUSTOM_FILM"){
        return [action.payload, ...customFilms]
    }
    return customFilms;
}