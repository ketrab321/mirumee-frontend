import { combineReducers } from 'redux';
import { customFilmsReducer, fetchedFilmsReducer } from './FilmReducers';
import { planetsReducer } from './PlanetsReducer';

const reducers = combineReducers({
    fetchedFilms: fetchedFilmsReducer,
    customFilms: customFilmsReducer,
    planets: planetsReducer
});

export default reducers;