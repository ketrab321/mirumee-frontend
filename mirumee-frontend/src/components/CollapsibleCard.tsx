import React from 'react';
import { StyleType } from '../App';
import Collapsible from './Collapsible';

type Props = {
    title: string
}

type State = {
    expanded: boolean
}

export default class CollapsibleCard extends React.Component<Props, State> {
    state: State = {
        expanded: false
    }

    style: StyleType = {
        card: {
            alignItems: "center",
            justifyContent: "center",
            minHeight: "48px",
            padding: "6px 20px 6px 20px",
        },
        collapsible: {
            background: "#FFFFFF",
            boxShadow: "0px 2px 1px rgba(196, 196, 196, 0.2)",
        },
        title: {
            background: "#FFFFFF",
            boxShadow: "0px 4px 2px rgba(196, 196, 196, 0.2)",
            borderRadius: "4px",

            position: "relative",
            display: 'flex',
            alignItems: "center",
            justifyContent: "flex-start",
            minHeight: "24px",
            padding: "20px",
            cursor: 'pointer'
        },
        arrow: {
            position: "absolute",
            right: "2vw"
        }
    }

    onTitleClick() {
        this.setState({
            expanded: !this.state.expanded
        })
    }

    render(){
        const arrow = this.state.expanded ? process.env.PUBLIC_URL + "/assets/ARROW_OPEN.svg" : process.env.PUBLIC_URL + "/assets/ARROW_CLOSE.svg";
        
        return (
            <div className="card" style={this.style.card}>
                <div className="card-content">
                    <div className="title" style={this.style.title} onClick={this.onTitleClick.bind(this)}>
                        {this.props.title}
                        <img src={arrow} style={this.style.arrow} alt="expand"/>
                    </div>
                </div>
                <Collapsible open={this.state.expanded} style={this.style.collapsible}>
                    {this.props.children}
                </Collapsible>
            </div>
        )
    }
}