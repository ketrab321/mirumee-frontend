import React from 'react';
import CSS from 'csstype';

type Props = {
    open: boolean,
    style?: CSS.Properties,
    className?: string,
}

export default class Collapsible extends React.Component<Props> {
    style: CSS.Properties = {
        overflow: "hidden",
        position: "relative",
        border: "1px solid #DDDDDD",
        borderRadius: "6px",
        padding: "1vw"
    }

    render(){
        const style: CSS.Properties = {
            ...this.props.style,
            ...this.style,
        }
        if(!this.props.open){
            return null;
        }
        return (
            <div className={this.props.className} style={style}>
                <div className="collapsible-content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}