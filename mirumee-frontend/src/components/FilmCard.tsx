import React from 'react';
import { Film } from '../reducers/FilmReducers';
import CollapsibleCard from './CollapsibleCard';
import PlanetList from './PlanetList';

type Props = {
    film: Film
}

export default class FilmCard extends React.Component<Props> {
    render(){        
        return (
            <CollapsibleCard title={this.props.film.title}>
                <PlanetList planets={this.props.film.planets}/>
            </CollapsibleCard>
        )
    }
}