import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { StyleType } from '../App';
import { RootState } from '../store';
import { fetchPlanets } from '../actions/PlanetsActions';
import Loader from './Loader';
import SortableTable from './SortableTable/SortableTable';

type Props = {
    planets: Array<string>
} & ReduxProps

class PlanetList extends React.Component<Props> {
    style: StyleType = {
        list: {
            width: "100%",
            display: 'flex',
            flexFlow: 'column',
            minHeight: '10vw',
            justifyContent: 'center'
        },
        loader: {
            width: "5vw",
            height: "5vw",
            alignSelf: 'center'
        }
    }

    areAllPlanetsFetched() {
        const fetchedPlanets = Object.keys(this.props.planetsMap);
        const notFetchedPlanets = this.props.planets.filter(planet => !fetchedPlanets.includes(planet));
        if(notFetchedPlanets.length > 0){
            this.props.fetchPlanets(notFetchedPlanets);
            return false;
        }
        return true;
    }

    render(){
        if(this.areAllPlanetsFetched()){
            const planets = this.props.planets.map(planet => {
                return this.props.planetsMap[planet];
            });
            return (
                <SortableTable data={planets} schema={{
                    name: "Planet name",
                    rotationPeriod: "Rotation period",
                    orbitalPeriod: "Orbital period",
                    diameter: "Diameter",
                    climate: "Climate",
                    surfaceWater: "Surface water",
                    population: "Population",
                }}/>
            )
        }
        else {
            return (
                <div className="planet-list" style={this.style.list}>
                    <Loader style={this.style.loader}/>
                </div>
            )
        }
        
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        planetsMap: state.planets
    }
}

const mapActions = {
    fetchPlanets
}

const connector = connect(mapStateToProps, mapActions)
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(PlanetList)
