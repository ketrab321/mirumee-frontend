import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { RootState } from '../store';
import FilmCard from './FilmCard';
import { StyleType } from '../App';
import Loader from './Loader';
import CustomFilmForm from './CustomFilmForm';


class FilmList extends React.Component<ReduxProps> {
    style: StyleType = {
        films: {
            boxSizing: "border-box",
            width: "100%",
        },
        filmList: {
            boxSizing: "border-box",
            display: 'flex',
            flexDirection: "column",
            width: "100%",
            padding: "1vw",

            fontFamily: "Barlow",
            color: "#00687F",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: "18px",
            lineHeight: "19px",
        },
        divider: {
            borderTop: "3px dashed #bbb",
            marginTop: "1vh",
            marginBottom: "1vh"
        },
        loader: {
            width: "10vw",
            height: "10vw",
            alignSelf: 'center'
        }
    }

    renderFilms(){
        let fetchedFilms = (                
            <div className="film-list" style={this.style.filmList}>
                <Loader style={this.style.loader}/>
            </div>
        )
        if(this.props.fetchedFilms.length > 0) {
            fetchedFilms = (
                <div className="film-list" style={this.style.filmList}>
                    {
                        this.props.fetchedFilms.map((film, index) => <FilmCard key={`film-${index}`} film={film}/>)
                    }
                </div>
            );
        }

        return (
            <React.Fragment>
                {fetchedFilms}
                <div className="film-list" style={this.style.filmList}>
                    Custom movies:
                    {
                        this.props.customFilms.map(film => <FilmCard film={film}/>)
                    }
                    <div className="divider" style={this.style.divider}></div>
                    <CustomFilmForm />
                </div>
            </React.Fragment>
            
        )
    }

    render(){
        return (
            <div className="films" style={this.style.films}>
                {this.renderFilms()}
            </div>
        )
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        fetchedFilms: state.fetchedFilms,
        customFilms: state.customFilms
    }
}

const connector = connect(mapStateToProps);
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(FilmList);