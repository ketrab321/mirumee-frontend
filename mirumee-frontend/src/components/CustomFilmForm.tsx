import React from 'react';
import { StyleType } from '../App';
import axios, { AxiosRequestConfig } from 'axios';
import Collapsible from './Collapsible';
import CollapsibleCard from './CollapsibleCard';
import Loader from './Loader';
import { addCustomFilm } from "../actions/FilmsActions";
import { connect, ConnectedProps } from 'react-redux';

type Planet = {
    name: string,
    id: string
}

type State = {
    titleValue: string,
    planetValue: string,
    typingTimeout: ReturnType<typeof setTimeout> | undefined
    planetSearchResult: Array<Planet>,
    selectedPlanets: Array<Planet>,
    searching: boolean
}

class CustomFilmForm extends React.Component<ReduxProps, State> {
    state: State = {
        titleValue: "",
        planetValue: "",
        typingTimeout: undefined,
        planetSearchResult: [],
        selectedPlanets: [],
        searching: false
    }

    style: StyleType = {
        form: {
            display: "flex",
            margin: "1vw",
            flexFlow: "column",

            fontFamily: "Barlow",
            fontStyle: "normal",
            fontWeight: "normal",
            fontSize: "16px",
            lineHeight: "19px",
        },
        input: {
            border: "none",
            borderBottom: "2px solid #DDDDDD",
            marginTop: "1vh",
            height: "32px",
            fontSize: "18px",
        },
        label: {
            marginTop: "1vh",
        },
        button: {
            position: "absolute",
            background: "#1BA1BE",
            color: "#FFFFFF",
            margin: "2vw",
            border: "none",
            borderRadius: "4px",
            fontSize: "14px",
            fontWeight: "bold",
            height: "48px",
            cursor: "pointer",

            right: "0",
            bottom: "1vh"
        },
        buttonDesktop: {
            width: "30vw",
        },
        buttonMobile: {
            width: "calc(100% - 4vw)",
        },
        buttonContainer: {
            marginTop: "6vh",
            height: "48px",
        },
        movieSearch: {
            marginTop: "1vh",
            cursor: "pointer"
        },
        searchResult: {
            margin: "5px",
        },
        selectedPlanet: {
            margin: "5px",
            padding: "8px",
            border: "2px solid #DDDDDD",
            borderRadius: "40px",
            width: "fit-content",
            cursor: "pointer"
        },
        selectedPlanets: {
            display: "flex",
            width: "100%",
            flexWrap: "wrap"
        },
        titleErrorMessage: {
            color: "red",
            padding: "5px",
            borderRadius: '5px',
            border: "2px solid red",
            marginTop: "2px"
        }
    }

    onTitleInputChange(value: string) {
        this.setState({
            titleValue: value
        })
    }

    fetchPlanetSearchData() {
        (async () => {
            const axiosConfig: AxiosRequestConfig = {
                url: `?search=${this.state.planetValue}`,
                method: 'get',
                baseURL: 'https://swapi.dev/api/planets/'
            }
            const response = await axios(axiosConfig)
            if(response.status === 200){
                const result = response.data.results as Array<{name: string, url: string}>;
                const planets = result.map(item => {
                    const urlParts = item.url.split('/');
                    const id = urlParts[urlParts.length-2];
                    return {
                        id,
                        name: item.name
                    } as Planet
                })
                this.setState({
                    planetSearchResult: planets,
                    searching: false
                })
            }
            else {
                window.alert(`Error while fetching planets: ${response.statusText}`)
            }
        })();
    }

    onPlanetsInputChange(value: string) {
        this.setState({
            planetValue: value,
            searching: true,
        })
        if(this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
        }
        this.setState({
            typingTimeout: setTimeout(this.fetchPlanetSearchData.bind(this), 2000)
        }) 
    }

    selectPlanet(planet: Planet) {
        const selectedIds = this.state.selectedPlanets.map(planet => planet.id);
        if(!selectedIds.includes(planet.id)){
            this.setState({
                selectedPlanets: [planet, ...this.state.selectedPlanets]
            })
        }
    }

    removePlanet(planet: Planet) {
        const newSelectedPlanets = this.state.selectedPlanets.filter(p => p.id !== planet.id);
        this.setState({
            selectedPlanets: newSelectedPlanets
        })
    }

    createMovie() {
        if(this.state.titleValue.match(/[A-Z][A-Za-z]*/) && this.state.titleValue.length !== 0){
            this.props.addCustomFilm({
                title: this.state.titleValue,
                planets: this.state.selectedPlanets.map(planet => planet.id)
            })
        }
    }

    renderSelectedPlanets()  {
        return <div style={this.style.selectedPlanets}>
            {
                this.state.selectedPlanets.map(planet => {
                    return (
                        <div style={this.style.selectedPlanet} 
                            key={planet.name}
                            onClick={()=>{this.removePlanet(planet)}}
                            >
                            {planet.name}
                            <img style={
                                {
                                    marginLeft: "8px",
                                }
                            } src={process.env.PUBLIC_URL + "/assets/DELETE.svg"} alt="delete" />
                        </div>
                    )
                })
            }
        </div>
        
    }

    renderSearchResult() {
        if(this.state.searching){
            return (
                <Collapsible open={true} style={this.style.movieSearch}>
                    <Loader style={
                        {
                            width: "50px",
                            height: "50px"
                        }
                    }/>
                </Collapsible>
            )
        }
        else if(this.state.planetSearchResult.length > 0){
            return (
                <Collapsible 
                    open={true} 
                    style={this.style.movieSearch}
                >
                    {
                        this.state.planetSearchResult.map(planet => {
                            return (
                                <div className="planets-search-result" style={this.style.searchResult} 
                                    onMouseUp={()=>{this.selectPlanet(planet)}} 
                                    key={planet.name}
                                >
                                    {planet.name}
                                </div>
                            )
                        })
                    }
                </Collapsible>
            )
        }
    }

    renderTitleError() {
        if(!this.state.titleValue.match(/[A-Z][A-Za-z]*/) && this.state.titleValue.length !== 0){
            return (
                <div style={this.style.titleErrorMessage}>
                    Title needs to start with upper case.
                </div>
            );
        }
        else if(this.state.titleValue.length === 0){
            return (
                <div style={this.style.titleErrorMessage}>
                    Film title is required.
                </div>
            );
        }        
        return null
    }

    renderForm() {
        return (
            <form style={this.style.form}>
                <label htmlFor="film-title">Movie title:</label>
                <input 
                    id="film-title-input" 
                    key="film-title" 
                    placeholder="Movie title"
                    onChange={(event) => {this.onTitleInputChange(event.target.value)}}
                    value={this.state.titleValue}
                    style={this.style.input}
                    pattern="[A-Z]{1}[A-Za-z]*"
                    required
                />
                {this.renderTitleError()}
                <label style={this.style.label} htmlFor="planet">Planets:</label>
                {this.renderSelectedPlanets()}
                <input 
                    id="planets-search" 
                    key="planet" 
                    placeholder="Search for planets in database"
                    onChange={(event) => {this.onPlanetsInputChange(event.target.value)}}
                    value={this.state.planetValue}
                    style={{
                        ...this.style.input,
                        background: 'url("assets/SEARCH.svg") right no-repeat'
                    }}
                />
                {this.renderSearchResult()}
            </form>
        )
    }

    render(){     
        const buttonStyle = window.outerWidth < 750 ? {
            ...this.style.button,
            ...this.style.buttonMobile
        } : {
            ...this.style.button,
            ...this.style.buttonDesktop
        }
        return (
            <CollapsibleCard title={"Add custom movie"}>
                {this.renderForm()}
                <div style={this.style.buttonContainer}>
                    <button onClick={this.createMovie.bind(this)} style={buttonStyle}>ADD MOVIE</button>
                </div>
            </CollapsibleCard>      
        )
    }
}

const mapActions = {
    addCustomFilm
}

const connector = connect(undefined, mapActions);
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(CustomFilmForm);