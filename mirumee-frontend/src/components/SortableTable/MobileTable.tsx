import React from "react";
import { StyleType } from "../../App";

type Props = {
    data: Array<any>,
    /** @schema Map 'object key' to 'title' */ 
    schema: {
        [key: string]: string
    },
}

export default class MobileTable extends React.Component<Props> {
    style: StyleType = {
        table: {
            background: "#FFFFFF",
            alignItems: "center",
            justifyContent: "center",

            fontFamily: "Barlow",
            color: "#00687F",
            fontStyle: "normal",
            fontSize: "18px",
            lineHeight: "19px",

            boxSizing: "content-box",
            width: "100%",

            borderSpacing: 0,
        },
        itemSection: {
            marginBottom: "2vh",
        },
        propertyTitle: {
            padding: "1vw",
            overflow: "clip",
            border: "solid 1px #00687F11",

            fontWeight: 'bold'
        },
        propertyValue: {
            padding: "1vw",
            overflow: "clip",
            border: "solid 1px #00687F11",
        },
        evenRow: {
            width: "100%",
            background: "#FFFFFF",
        },
        oddRow: {
            width: "100%",
            background: "#CCCCCC",
        }
    }

    renderObjectProperties(index: number, item: any, keys: Array<string>) {
        const style = index % 2 === 0 ? this.style.evenRow : this.style.oddRow;

        return (
            <tbody className={`item-${index}`} key={`item-${index}`} style={this.style.itemSection}>
                {
                    keys.map(key => {
                        return (
                            <tr key={`row-${index}-${key}`} className="table-row" style={style}>
                                <td key={`row-${index}-${key}-title`} className="table-item" style={this.style.propertyTitle}>
                                    {this.props.schema[key]}
                                </td>
                                <td key={`row-${index}-${key}-value`} className="table-item" style={this.style.propertyValue}>
                                    {item[key]}
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        );
    }

    renderData() {
        return this.props.data.map((item, index) => {
                const keys = Object.keys(this.props.schema);
                return this.renderObjectProperties(index, item, keys)
            })
    }

    render() {
        return (
            <table className="sortable-table" style={this.style.table}>
                {this.renderData()}
            </table>
        )
    }
}