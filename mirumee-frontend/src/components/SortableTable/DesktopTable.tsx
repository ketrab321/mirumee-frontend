import React from "react";
import { StyleType } from "../../App";

type Props = {
    data: Array<any>,
    /** @schema Map 'object key' to 'title' */ 
    schema: {
        [key: string]: string
    },
    sortingKey: string,
    ordering: "ascending" | "descending",
    onSortingChange: (key: string, ordering: "ascending" | "descending") => void
}

export default class DesktopTable extends React.Component<Props> {
    style: StyleType = {
        table: {
            background: "#FFFFFF",
            alignItems: "center",
            justifyContent: "center",

            fontFamily: "Barlow",
            color: "#00687F",
            fontStyle: "normal",
            fontSize: "14px",
            lineHeight: "19px",

            boxSizing: "content-box",
            width: "100%",

            borderSpacing: 0,
        },
        tableRow: {
            width: "100%"
        },
        tableHeader: {
            fontSize: "16px",
            width: "100%",
            background: "#DDDDDD",
        },
        tableItem: {
            padding: "1vw",
            border: "solid 1px #00687F33",
            overflow: "clip"
        },
        headerTitle: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        arrows: {
            fontSize: "12px",  
            padding: "6px",
            cursor: 'pointer',
            color: "#008A9F44",   
        },
        clickedArrow: {
            color: "#00687F",
        }
    }

    setSorting(key: string, ordering: "ascending" | "descending") {
        if(key === this.props.sortingKey && ordering === this.props.ordering){
            this.props.onSortingChange("none", "ascending")
        }
        else {
            this.props.onSortingChange(key, ordering)
        }
    }

    renderSortingButtons(key: string) {
        const upArrowStyle = (key === this.props.sortingKey && 
                            this.props.ordering === "ascending") ? 
                            this.style.clickedArrow : 
                            {};
        const downArrowStyle = (key === this.props.sortingKey && 
                            this.props.ordering === "descending") ? 
                            this.style.clickedArrow : 
                            {};                   
        return (
            <div className="sorting-buttons" style={this.style.arrows}>
                <div 
                    style={upArrowStyle} 
                    onClick={()=>{this.setSorting(key, "ascending")}}
                >
                    &#9650;
                </div>
                <div 
                    style={downArrowStyle} 
                    onClick={()=>{this.setSorting(key, "descending")}}
                >
                    &#9660;
                </div>
            </div>
        )
    }

    renderHeader() {
        const keys = Object.keys(this.props.schema);

        return (
            <thead>
                <tr className="table-row" style={this.style.tableHeader}>
                    {
                        keys.map(key => {
                            return (
                                <th key={`header-${key}`} className="table-header-item" style={this.style.tableItem}>
                                    
                                    <div className="header-title" style={this.style.headerTitle}>
                                        {this.props.schema[key]} 
                                        {this.renderSortingButtons(key)}
                                    </div>
                                </th>
                            )
                        })
                    }
                </tr>
            </thead>
        )
    }

    renderObjectProperties(index: number, item: any, keys: Array<string>) {
        return (
            <tr key={`row-${index}`} className="table-row" style={this.style.tableRow}>
                {
                    keys.map(key => {
                        return (
                            <td key={`row-${index}-${key}`} className="table-item" style={this.style.tableItem}>
                                {item[key]}
                            </td>
                        )
                    })
                }
            </tr>
        )
    }

    renderData() {
        return (
            <tbody>
                {
                    this.props.data.map((item, index) => {
                        const keys = Object.keys(this.props.schema);
                        return this.renderObjectProperties(index, item, keys)
                    })
                }
            </tbody>
        )
    }

    render() {
        return (
            <table className="sortable-table" style={this.style.table}>
                {this.renderHeader()}
                {this.renderData()}
            </table>
        )
    }
}