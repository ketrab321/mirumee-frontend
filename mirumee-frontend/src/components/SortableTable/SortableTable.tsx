import React from "react";
import DesktopTable from "./DesktopTable";
import MobileTable from "./MobileTable";

type Props = {
    data: Array<any>,
    /** @schema Map 'object key' to 'title' */ 
    schema: {
        [key: string]: string
    },
}

type State = {
    sortingKey: string,
    ordering: "ascending" | "descending",
    width: number
}

export default class SortableTable extends React.Component<Props, State> {
    state: State = {
        sortingKey: "none",
        ordering: "ascending",
        width: 0
    }

    tableRef: React.RefObject<HTMLInputElement>;

    constructor(props: Props){
        super(props);
        this.tableRef = React.createRef();
    } 

    componentDidMount() {
        if(this.tableRef){
            let width = 0;
            this.tableRef.current?.childNodes?.forEach(node => {
                width += (node as HTMLInputElement).clientWidth;
            })
            this.setState({ width });
        }
    }

    setSorting(key: string, ordering: "ascending" | "descending"){
        this.setState({
            sortingKey: key,
            ordering: ordering
        })
    }

    compareItems(item1: any, item2: any){
        if(typeof item1[this.state.sortingKey] === "number"){
            return (item1[this.state.sortingKey] as number) - (item2[this.state.sortingKey] as number);
        }
        else if(typeof item1[this.state.sortingKey] === "string"){
            return (item1[this.state.sortingKey] as string).localeCompare(item2[this.state.sortingKey])
        }
        else if(typeof item1[this.state.sortingKey] === "boolean"){
            if(item1[this.state.sortingKey] === item2[this.state.sortingKey]){
                return 0;
            }
            else if(item1[this.state.sortingKey] === true && item2[this.state.sortingKey] === false){
                return 1;
            }
            else {
                return -1;
            }
        }
        else if(typeof item1[this.state.sortingKey] === "object"){
            return 1;
        }
        else {
            return 0;
        }
    }

    sortData() {
        const sampleItem = this.props.data[0];
        if(Object.keys(sampleItem).includes(this.state.sortingKey)){
            let sortedData = [...this.props.data].sort((item1, item2) => {
                let result = this.compareItems(item1, item2)
                if(this.state.ordering === "ascending"){
                    return result;
                }
                else {
                    return (-1)*result;
                }
            })
            return sortedData;
        }
        else {
            return this.props.data;
        }
    }

    isTableTooBig() {
        return window.innerWidth < 750;
    }

    render() {
        let data = this.sortData();

        const desktopContainer = (
            <DesktopTable 
                data={data} 
                schema={this.props.schema} 
                sortingKey={this.state.sortingKey} 
                ordering={this.state.ordering}
                onSortingChange={this.setSorting.bind(this)}
            />
        )
        const mobileContainer = (
            <MobileTable 
                data={data} 
                schema={this.props.schema} 
            />
        )

        const dataContainer = this.isTableTooBig() ? mobileContainer : desktopContainer;
        return (
            <div className="sortable-table" ref={this.tableRef}>
                {dataContainer}
            </div>
        )
    }
}