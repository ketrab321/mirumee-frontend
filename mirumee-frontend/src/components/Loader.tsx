import React from "react";
import CSS from 'csstype';

type Props = {
    style?: CSS.Properties
}
export default class Loader extends React.Component<Props> {
    render() {
        return (
            <React.Fragment>
                <style>
                    {
                        `
                            .loader {
                                animation: Spin 1.5s infinite linear;
                            }
                            @keyframes Spin {
                                0% {
                                    transform: rotate(0deg);
                                }
                                100% {
                                    transform: rotate(-360deg);
                                }
                            }
                        `
                    }
                </style>
                <img className="loader" style={this.props.style} src={process.env.PUBLIC_URL +"/assets/LOADER.svg"} alt="loader"/>
            </React.Fragment>
        )
    }
}