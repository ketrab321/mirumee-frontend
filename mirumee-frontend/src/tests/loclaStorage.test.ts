import { addCustomFilm, loadCustomFilms, CUSTOM_FILMS_LOCAL_STORAGE_KEY } from '../actions/FilmsActions';
import LocalStorageMock, { LocalStorage } from './LocalStorageMock';
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';
import reducers from '../reducers';

describe("Local storage", ()=>{
  it("Tests loading custom films from local storage", ()=>{
    const initialStorage = [
      { title: 'A New Hope', planets: ["1", "2", "3"] },
      { title: 'The Empire Strikes Back', planets: ["4", "5", "6", "27"] },
      { title: 'Return of the Jedi', planets: ["1", "5", "7", "8", "9"] },
      { title: 'The Phantom Menace', planets: ["1", "8", "9"] },
      { title: 'Attack of the Clones', planets: ["1", "8", "9", "10", "11"] },
      { title: 'Revenge of the Sith', planets: ["1", "2", "5", "8", "9", "12", "13", "14", "15", "16", "17", "18", "19"] }
    ]
    const serialized = JSON.stringify(initialStorage);
    const mock = new LocalStorageMock({
      [CUSTOM_FILMS_LOCAL_STORAGE_KEY]: serialized
    });
    Object.defineProperty(window, 'localStorage', { value: mock });


    expect(localStorage.getItem(CUSTOM_FILMS_LOCAL_STORAGE_KEY)).toEqual(serialized);
    expect(loadCustomFilms()).toEqual({
      type: 'LOAD_CUSTOM_FILMS',
      payload: initialStorage
    });
  })
  it("Tests saving custom film to localStorage", ()=>{
    const store = createStore(reducers, applyMiddleware(thunk));

    const mock = new LocalStorageMock({
      [CUSTOM_FILMS_LOCAL_STORAGE_KEY]: "[]"
    });
    Object.defineProperty(window, 'localStorage', { value: mock });
    const customFilmTitle = "Title";
    const customFilmPlanets = ["1", "3"];

    const customFilm = {
      title: customFilmTitle,
      planets: customFilmPlanets
    }

    return addCustomFilm(customFilm)(store.dispatch, store.getState, undefined).then(() => {
      expect(store.getState().customFilms.length).toEqual(1);
      expect(store.getState().customFilms[0]).toEqual(customFilm);
      expect(localStorage.getItem(CUSTOM_FILMS_LOCAL_STORAGE_KEY)).toEqual(JSON.stringify([customFilm]))
    })
  })
});