export type LocalStorage = {
    [key: string]: string;
}

export default class LocalStorageMock implements Storage {
    store: LocalStorage = {}
    constructor(initialStorage: LocalStorage = {}) {
        this.store = initialStorage;
    }

    get length(){
        return Object.keys(this.store).length
    }

    key(index: number): string | null {
        throw new Error('Method not implemented.');
    }
    clear() {
        this.store = {};
    }

    getItem(key: string) {
        return this.store[key] || null;
    }

    setItem(key: string, value: string) {
        this.store[key] = String(value);
    }

    removeItem(key: string) {
        delete this.store[key];
    }

    getStorage() {
        return this.store;
    }
};
  