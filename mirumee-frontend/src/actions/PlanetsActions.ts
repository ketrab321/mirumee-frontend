import axios, { AxiosRequestConfig } from 'axios';
import { ThunkAction } from 'redux-thunk';
import { Planets, Planet } from '../reducers/PlanetsReducer';
import { RootState } from '../store';

type ThunkResult<R> = ThunkAction<R, RootState, undefined, PlanetAction>;
export type PlanetAction = {
    type: "FETCH_PLANETS",
    payload: Planets
}

type FetchedPlanet = {
    name: string,
	rotation_period: string,
	orbital_period: string,
	diameter: string,
	climate: string,
	gravity: string,
	terrain: string,
	surface_water: string,
	population: string,
}

function mapProperty(prop: string | undefined) {
    return prop ? prop : "unknown";
}

function parseProperty(prop: string | undefined) {
    return prop && prop !== "unknown" ? parseFloat(prop) : "unknown";
}

function mapFetchPlanet(fetchedPlanet: FetchedPlanet): Planet {
    return {
        name: mapProperty(fetchedPlanet.name),
        rotationPeriod: parseProperty(fetchedPlanet.rotation_period),
        orbitalPeriod:  parseProperty(fetchedPlanet.orbital_period),
        diameter:  parseProperty(fetchedPlanet.diameter),
        climate:  mapProperty(fetchedPlanet.climate),
        gravity:  mapProperty(fetchedPlanet.gravity),
        terrain:  mapProperty(fetchedPlanet.terrain),
        surfaceWater:  parseProperty(fetchedPlanet.surface_water),
        population: parseProperty(fetchedPlanet.population),
    }
}

export function fetchPlanets(ids: Array<string>): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const fetchedPlanets: Planets = {}

        for (const id of ids) {
            const axiosConfig: AxiosRequestConfig = {
                url: `${id}/`,
                method: 'get',
                baseURL: 'https://swapi.dev/api/planets/'
            }
            const response = await axios(axiosConfig)
            if(response.status === 200){
                const result = response.data as FetchedPlanet;   
                const newPlanet = mapFetchPlanet(result);
                fetchedPlanets[`${id}`] = newPlanet;
            }
            else {
                window.alert(`Error while fetching planets: ${response.statusText}`)
            }
        }
        
        dispatch({
            type: "FETCH_PLANETS",
            payload: fetchedPlanets
        })
    }
}