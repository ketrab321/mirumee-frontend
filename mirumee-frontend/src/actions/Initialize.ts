import { ThunkAction } from 'redux-thunk';
import { RootState } from '../store';
import { fetchFilms, FilmActions, loadCustomFilms } from '../actions/FilmsActions';

type ThunkResult<R> = ThunkAction<R, RootState, undefined, FilmActions>;

export default function initialize(): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        dispatch(loadCustomFilms());
        const  asyncCall =  fetchFilms();
        await asyncCall(dispatch, getState, undefined);
    }
}