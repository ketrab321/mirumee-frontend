import axios, { AxiosRequestConfig } from 'axios';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../store';
import { Film } from '../reducers/FilmReducers';

export const CUSTOM_FILMS_LOCAL_STORAGE_KEY = "mirumee_sw_customFilms"

type ThunkResult<R> = ThunkAction<R, RootState, undefined, FilmActions>;
export type FilmActions = FetchFilmsAction | LoadCustomFilmsAction | AddFilmAction;

export type FetchFilmsAction = {
    type: "FETCH_FILMS",
    payload: Array<Film>
}

export type LoadCustomFilmsAction = {
    type: "LOAD_CUSTOM_FILMS",
    payload: Array<Film>
}

export type AddFilmAction = {
    type: "NEW_CUSTOM_FILM",
    payload: Film
}

export function fetchFilms(): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const axiosConfig: AxiosRequestConfig = {
            url: 'films/',
            method: 'get',
            baseURL: 'https://swapi.dev/api/'
        }
        const response = await axios(axiosConfig)
        if(response.status === 200){
            const result = response.data.results as Array<Film>;

            const films = result.map(item => {
                const planets = item.planets.map(planetURL => {
                    const urlParts = planetURL.split('/');
                    const planetId = urlParts[urlParts.length - 2];
                    return planetId;
                });
                return {
                    title: item.title,
                    planets: planets
                }
            });
            dispatch({
                type: "FETCH_FILMS",
                payload: films
            })
        }
        else {
            window.alert(`Error while fetching films: ${response.statusText}`)
        }
    }
}

export function loadCustomFilms(): FilmActions {
    const serializedFilms = localStorage.getItem(CUSTOM_FILMS_LOCAL_STORAGE_KEY);
    const films = serializedFilms ? JSON.parse(serializedFilms) : [];
    return {
        type: "LOAD_CUSTOM_FILMS",
        payload: films
    }
}

export function addCustomFilm(film: Film): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const { customFilms } = getState();
        let newCustomFilms = [film];
        if(customFilms){
            newCustomFilms = [film, ...customFilms];
        }
        localStorage.setItem(CUSTOM_FILMS_LOCAL_STORAGE_KEY, JSON.stringify(newCustomFilms));

        dispatch({
            type: "NEW_CUSTOM_FILM",
            payload: film
        })
    }
}