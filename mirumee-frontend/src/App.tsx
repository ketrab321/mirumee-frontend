import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import CSS from 'csstype';
import initialize from './actions/Initialize';
import FilmList from './components/FilmList';


class App extends React.Component<ReduxProps> {
    constructor(props: ReduxProps){
        super(props);
        props.initialize();
    }

    style: StyleType = {
        app: {
            display: 'flex',
            flexDirection: "column",
            minWidth: "100vw",
            minHeight: "100vh",
            background: "#E0E6EE",
            alignItems: "center",
        },
        logo: {
            margin: "10vw",
            width: "50vw"
        }
    }

    render(){
        return (
            <div className="app" style={this.style.app}>
                <img style={this.style.logo} src={process.env.PUBLIC_URL + "/assets/LOGO.svg"} alt="Logo"/>
                <FilmList></FilmList>
            </div>
        )
    }
}


export type StyleType = {
    [key: string]: CSS.Properties;
}

const mapActions = {
    initialize
}
const connector = connect(undefined, mapActions);
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(App);