import { Selector, ClientFunction } from 'testcafe';

fixture `End to end testing`
    .page `http://localhost:3000`


const elementVisibilityWatcher = (element) =>{
    return ClientFunction(() => {
        return new Promise(resolve => {
            var interval = setInterval(() => {
                if (document.querySelector(element))
                    return;
    
                clearInterval(interval);
                resolve();
            }, 100);
        });
    });
} 

test('Creating custom film', async t => {
    await t
        .click(Selector(".title").withText("Add custom movie"))
        .typeText('#film-title-input', "Test title")
        .typeText('#planets-search', "Hoth");

    const watcher = elementVisibilityWatcher(".planets-search-result")
    await watcher;
    await t
        .click(Selector(".planets-search-result").nth(0))
        .click(Selector("button").withText("ADD MOVIE"))
})